package com.example.mydbofproductsapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    TableProducts tableOfProducts;
    ListView listViewProducts;



    private void UpdateListViewProducts()
    {
        ProductAdapter productAdapter = new ProductAdapter(tableOfProducts.getAll(), this);
        listViewProducts.setAdapter(productAdapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tableOfProducts= new TableProducts(getApplicationContext());

        listViewProducts=findViewById(R.id.listViewProducts);

        UpdateListViewProducts();
    }
}