package com.example.mydbofproductsapp;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class TableProducts
{
    private DbHelper dbHelper;

    public TableProducts(Context context)
    {
        this.dbHelper = new DbHelper(context);
    }

    public ArrayList<Product> getAll()
    {
        ArrayList<Product> products = new ArrayList<>();

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String sqlCommand = "SELECT * FROM `products`";

        Cursor cursor = db.rawQuery(sqlCommand, null);

        while (cursor.moveToNext()==true)
        {
            Product product = new Product(
                    cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getInt(2)
            );

            products.add(product);
        }

        cursor.close();

        dbHelper.close();

        return products;
    }
}
